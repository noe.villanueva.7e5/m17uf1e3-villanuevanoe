using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    public Text Frames;
    private int frameRate = 0;
    public PlayerMovement playerMovement;
    public Hability hability;
    public DataPlayer playerData;
    public SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Frames.text = frameRate.ToString() + " FPS";
        frameRate++;
    }

    public void ActiveHability()
    {
        if (hability.modesHability == HabilityModes.Avaliable) hability.modesHability = HabilityModes.Growing;
    }

    public void ChangeDependency()
    {
        if (playerData.movementDependency == MovementDependency.Automatic) playerData.movementDependency = MovementDependency.Manual;
        else if (playerData.movementDependency == MovementDependency.Manual)
        {
            playerMovement.movementDirection = 1;
            sprite.flipX = true;
            playerData.movementDependency = MovementDependency.Automatic;
            playerData.movementState = MovementState.Walking;
        } 
    }
}
