using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataPlayer : MonoBehaviour
{
    public string PlayerName;
    public Text nameText;
    public Sprite[] PlayerSprites;
    public float Height;
    public float Weight;
    public float Speed;
    public float Distance;
    public PlayerType PlayerType;
    public Text PlayerTypeText;
    public MovementState movementState;
    public MovementDependency movementDependency;

    // Start is called before the first frame update
    void Start()
    {
        PlayerName = PlayerPrefs.GetString("Name");
        //Debug.Log(PlayerName);
        nameText.text = PlayerName;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerTypeText.text = "( " + PlayerType.ToString() + " )";
    }
}

public enum PlayerType
{
    Warrior,
    Knight,
    Mage,
    Assasin
}

public enum MovementState
{
    Walking,
    OnGuard
}

public enum MovementDependency
{
    Automatic,
    Manual
}