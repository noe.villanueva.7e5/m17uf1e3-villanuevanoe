using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private SpriteRenderer sprite;
    private DataPlayer playerData;
    private Transform movement;
    private float distance;
    public float movementDirection = 1;
    private float onGuardTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
        playerData = gameObject.GetComponent<DataPlayer>();
        movement = gameObject.GetComponent<Transform>();
        distance = playerData.Distance;
        sprite.flipX = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerData.movementDependency == MovementDependency.Automatic) AutomaticGuardPosition();
        else if (playerData.movementDependency == MovementDependency.Manual) PlayerManualWalking();
    }

    private void PlayerMove(float movementDirection)
    {
        movement.position = new Vector3(movement.position.x + (movementDirection * playerData.Speed / playerData.Weight ), movement.position.y, movement.position.z);
    }

    private void AutomaticGuardPosition()
    {
        if (playerData.movementState == MovementState.Walking)
        {
            PlayerMove(movementDirection);
            distance -= playerData.Speed / playerData.Weight;
            Debug.Log(distance);
            if (distance < 0 )
            {
                distance = playerData.Distance;
                movementDirection *= -1;

                if (movementDirection == 1)
                {
                    playerData.movementState = MovementState.OnGuard;
                    onGuardTime = 15;
                }

                sprite.flipX = !sprite.flipX;
            }
        }
        else if (playerData.movementState == MovementState.OnGuard)
        {
            onGuardTime--;
            if (onGuardTime == 0) playerData.movementState = MovementState.Walking;
        }
    }

    private void PlayerManualWalking()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            sprite.flipX = true;
            playerData.movementState = MovementState.Walking;
            PlayerMove(1);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            sprite.flipX = false;
            playerData.movementState = MovementState.Walking;
            PlayerMove(-1);
        }
        else
        {
            playerData.movementState = MovementState.OnGuard;
        }
    }
}
