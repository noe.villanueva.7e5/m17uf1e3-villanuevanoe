using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    private Sprite[] animacio;
    private SpriteRenderer spriteRenderer;
    private DataPlayer playerData;
    private int spriteIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        animacio = gameObject.GetComponent<DataPlayer>().PlayerSprites;
        playerData = gameObject.GetComponent<DataPlayer>();
        Application.targetFrameRate = 12;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerData.movementState == MovementState.Walking) SpriteWalking();
        else if (playerData.movementState == MovementState.OnGuard) SpriteOnGuard();
    }

    private void SpriteWalking()
    {
        spriteRenderer.sprite = animacio[spriteIndex];
        spriteIndex++;

        if (spriteIndex == animacio.Length)
        {
            spriteIndex = 0;
        }
    }

    private void SpriteOnGuard()
    {
        spriteRenderer.sprite = animacio[1];
    }
}
